### Fontes SVG Customizadas
> Siga os procedimentos abaixo para importar a fonte ao seu projeto

#### Para importar ao seu projeto:
```
gem 'jefonts', git: 'https://gitlab.tre-ro.jus.br/resources/jefonts'
```

#### Feito isto, agora basta mapear os arquivos em seu projeto

##### No arquivo **applicattion.css ou .scss**, adicione o código abaixo:
**.css**:
```
/*
*=require jefonts
*/
```
**.scss**:
```
@import 'jefonts';
```
#### Para chamar o ícone em sua página HTML basta colocar isto:
```
<i class="icon-nome_da_fonte"></i>

# Exemplo:

<i class="icon-urna"></i>
```
#### Para aumentar o tamanho do ícone:
```
* .icon-xs
* .icon-sm
* .icon-lg
* .icon-2x
* ...
* .icon-10x
```
Exemplo de uso:
```
<i class="icon-urna icon-lg"></i>
```
### Para criar uma fonte
Crie um arquivo **SVG** e abra o site [Fontello.com](http://fontello.com/), após isso araste o seu arquivo para a seção **Custom Icons** que aparece no site, feito isto você poderá realizar algumas configurações para o arquivo, por fim selecione o ícone que deseja baixar e clique no botão **Download webfont**.

#### Salve o seu ícone
Realize um **git pull** ou **git clone** deste projeto e adicione os arquivos da fonte ao projeto, obedecendo os padrões, e apos isto de um **git push** e pronto.
